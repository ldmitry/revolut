package com.apps.likhodedov.revolut.currencyrates.presenter

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.res.ResourcesCompat
import com.apps.likhodedov.revolut.currencyrates.view.CurrencyRateItem
import com.apps.likhodedov.revolut.data.db.CurrencyRate
import org.json.JSONArray
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

/** Providing full information about currency. */
class CurrencyItemInfoProvider(
    /** app context. */
    private val context: Context
) {

    /** Contains all needed info about currency. */
    inner class CurrencyInfo(
        /** Currency code. Ex: USD. */
        val currencyCode: String,
        /** Currency name. Ex: U.S. Dollar. */
        val currencyName: String,
        /** Drawable id of the flag for this currency. */
        val flagForCurrencyDrawableId: Int
    )

    private val currencyInfoItems: List<CurrencyInfo>

    init {
        val assetString = readCountryToCurrencyAssetFileToString()
        currencyInfoItems = processJsonStringToCurrencyItems(assetString)
    }

    /** Get currency rate item by currency rate. */
    fun getCurrencyRateItem(currencyRate: CurrencyRate): CurrencyRateItem {
        val item = currencyInfoItems.find { it.currencyCode == currencyRate.currencyCode }!!
        return CurrencyRateItem(
            getDrawable(item.flagForCurrencyDrawableId),
            item.currencyCode,
            item.currencyName,
            currencyRate.rate
        )
    }

    private fun getDrawableResId(countryCode: String): Int {
        val uri = COUNTRY_DRAWABLE_PREFIX + countryCode.toLowerCase()
        return context.resources.getIdentifier(uri, null, context.packageName)
    }

    private fun getDrawable(id: Int): Drawable {
        return ResourcesCompat.getDrawable(context.resources, id, null)!!
    }

    private fun processJsonStringToCurrencyItems(assetString: String): List<CurrencyInfo> {
        val items = ArrayList<CurrencyInfo>()
        val jsonArray = JSONArray(assetString)
        for (i in 0 until jsonArray.length()) {
            val item = jsonArray.getJSONObject(i)
            items.add(
                CurrencyInfo(
                    item.getString(CURRENCY_CODE_NAME_IDENTIFIER),
                    item.getString(CURRENCY_NAME_IDENTIFIER),
                    getDrawableResId(item.getString(COUNTRY_CODE_NAME_IDENTIFIER))
                )
            )
        }
        return items
    }

    private fun readCountryToCurrencyAssetFileToString(): String {
        val inputStream: InputStream
        val sb = StringBuilder()
        try {
            inputStream = context.assets.open(COUNTRY_TO_CURRENCY_FILE_PATH)
            val br = BufferedReader(InputStreamReader(inputStream))
            var line: String? = br.readLine()
            while (line != null) {
                sb.append(line)
                line = br.readLine()
            }
            br.close()
        } catch (e: IOException) {
            throw IOException("Something wrong with asset file")
        }
        return sb.toString()
    }

    private companion object {
        private const val COUNTRY_TO_CURRENCY_FILE_PATH = "countries/CountryToCurrency.json"
        private const val COUNTRY_DRAWABLE_PREFIX = "drawable/ic_"
        private const val COUNTRY_CODE_NAME_IDENTIFIER = "countryCode"
        private const val CURRENCY_NAME_IDENTIFIER = "currencyName"
        private const val CURRENCY_CODE_NAME_IDENTIFIER = "currencyCode"
    }
}