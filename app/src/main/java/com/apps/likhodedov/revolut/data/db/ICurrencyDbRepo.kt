package com.apps.likhodedov.revolut.data.db

import io.reactivex.Observable

/** Repository interface for currency rate database. */
interface ICurrencyDbRepo {

    /** Get currency Rates. */
    fun getCurrencyRates(): Observable<List<CurrencyRate>>

    /** Insert [currencyRates] to database. */
    fun insertCurrencies(currencyRates: List<CurrencyRate>): Observable<Boolean>
}