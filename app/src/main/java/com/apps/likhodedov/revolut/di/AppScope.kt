package com.apps.likhodedov.revolut.di

import javax.inject.Scope

/** Application scope. */
@Scope
annotation class AppScope