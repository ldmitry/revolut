package com.apps.likhodedov.revolut.di.activity

import com.apps.likhodedov.revolut.MainActivity
import com.apps.likhodedov.revolut.data.db.CurrencyDao
import com.apps.likhodedov.revolut.data.db.CurrencyDbRepo
import com.apps.likhodedov.revolut.data.db.ICurrencyDbRepo
import com.apps.likhodedov.revolut.data.network.CurrencyRateApi
import com.apps.likhodedov.revolut.data.network.CurrencyRateWebRepo
import com.apps.likhodedov.revolut.data.network.ICurrencyRateWebRepo
import com.apps.likhodedov.revolut.di.ActivityScope
import dagger.Module
import dagger.Provides

/** Activity main module. */
@Module
class ActivityMainModule(activity: MainActivity) {

    /** Provide database repository. */
    @ActivityScope
    @Provides
    fun provideCurrencyDbRepo(currencyDaoSession: CurrencyDao): ICurrencyDbRepo {
        return CurrencyDbRepo(currencyDaoSession)
    }

    /** Provide web repository. */
    @ActivityScope
    @Provides
    fun provideCurrencyWebRepo(): ICurrencyRateWebRepo {
        return CurrencyRateWebRepo(CurrencyRateApi.Factory.create())
    }
}