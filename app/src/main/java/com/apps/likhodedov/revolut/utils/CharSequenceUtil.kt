package com.apps.likhodedov.revolut.utils

/** Convert char sequence to double. */
fun CharSequence.toDouble(): Double {
    return if (this.isEmpty()) {
        0.0
    } else {
        val doubleNumber = this.toString().toDoubleOrNull()
        requireNotNull(doubleNumber) { "Charsequence is not a number" }
        doubleNumber
    }
}