package com.apps.likhodedov.revolut.di.currencyfragment

import com.apps.likhodedov.revolut.currencyrates.view.CurrencyRateFragment
import com.apps.likhodedov.revolut.di.FragmentScope
import dagger.Subcomponent

/** Currency rates fragment component. */
@FragmentScope
@Subcomponent(modules = [CurrencyFragmentModule::class])
interface CurrencyFragmentComponent {

    /** Initialize fragment [fragment]. */
    fun inject(fragment: CurrencyRateFragment)
}