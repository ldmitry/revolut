package com.apps.likhodedov.revolut.currencyrates.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apps.likhodedov.revolut.currencyrates.presenter.ICurrencyRatePresenter
import com.apps.likhodedov.revolut.di.currencyfragment.ICurrencyFragmentComponentFactory
import com.apps.likhodedov.revolut.utils.findComponentFactory
import kotlinx.android.synthetic.main.currency_fragment.*
import javax.inject.Inject
import androidx.recyclerview.widget.SimpleItemAnimator
import com.apps.likhodedov.revolut.R

/** Fragment, which displaying currency rates. */
class CurrencyRateFragment : Fragment(), ICurrencyRateView, CurrencyRateAdapter.Listener {

    @Inject
    lateinit var presenter: ICurrencyRatePresenter

    private lateinit var adapter: CurrencyRateAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        findComponentFactory<ICurrencyFragmentComponentFactory>()
            .createCurrencyFragment(this)
            .inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.currency_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated()

        adapter = CurrencyRateAdapter(context!!, this)

        val animator = recyclerView.itemAnimator
        if (animator is SimpleItemAnimator) {
            animator.supportsChangeAnimations = false
        }
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> presenter.onRatesScrollStopped()
                    RecyclerView.SCROLL_STATE_DRAGGING,
                    RecyclerView.SCROLL_STATE_SETTLING -> presenter.onRatesScrolling()
                }
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
    }

    override fun onPause() {
        super.onPause()
        presenter.onViewPaused()
    }

    override fun onResume() {
        super.onResume()
        presenter.onViewResumed()
    }

    override fun updateCurrencyRates(
        currencyRateItems: List<CurrencyRateItem>,
        currentCurrencyRateItem: CurrencyRateItem,
        currentItemMoneyCount: Double
    ) {
        adapter.updateCurrencyRates(
            currencyRateItems,
            currentCurrencyRateItem,
            currentItemMoneyCount
        )
    }

    override fun setNoDataInfoVisible(visible: Boolean) {
        if (visible) {
            info_container.visibility = View.VISIBLE
        } else {
            info_container.visibility = View.INVISIBLE
        }
    }

    override fun setCurrencyRatesInfoVisible(visible: Boolean) {
        if (visible) {
            recyclerView.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.INVISIBLE
        }
    }

    override fun setLoaderVisible(visible: Boolean) {
        if (visible) {
            data_progress_bar.visibility = View.VISIBLE
        } else {
            data_progress_bar.visibility = View.INVISIBLE
        }
    }

    override fun onCurrencyItemClicked(currencyRateItem: CurrencyRateItem, money: Double) {
        presenter.onCurrencyRateClicked(currencyRateItem, money)
    }

    override fun onCurrentCurrencyMoneyChanged(money: Double) {
        presenter.onCurrencyRateMoneyChangedByUser(money)
    }
}