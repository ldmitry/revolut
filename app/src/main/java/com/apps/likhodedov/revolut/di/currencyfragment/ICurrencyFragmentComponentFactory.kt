package com.apps.likhodedov.revolut.di.currencyfragment

import com.apps.likhodedov.revolut.currencyrates.view.ICurrencyRateView
import com.apps.likhodedov.revolut.di.IComponentFactory

/** Factory for currency fragment component. */
interface ICurrencyFragmentComponentFactory : IComponentFactory {

    /** Create currency fragment component. */
    fun createCurrencyFragment(fragment: ICurrencyRateView): CurrencyFragmentComponent
}