package com.apps.likhodedov.revolut.data.network

import io.reactivex.Observable

/** Repository of currency rates from internet. */
class CurrencyRateWebRepo(
    /** API for getting currency rates. */
    private val api: CurrencyRateApi
) : ICurrencyRateWebRepo {

    override fun getCurrencyRates(): Observable<ArrayList<WebRate>> {
        return api.getCurrencyRates()
    }
}