package com.apps.likhodedov.revolut.data.db

import io.reactivex.Observable

/** Repository for currency rates database. */
class CurrencyDbRepo(
    /** Session for interacting with currency rates database. */
    private val currencyDaoSession: CurrencyDao
) : ICurrencyDbRepo {
    override fun insertCurrencies(currencyRates: List<CurrencyRate>): Observable<Boolean> {
        return Observable.fromCallable {
            currencyDaoSession.insert(currencyRates)
            return@fromCallable true
        }
    }

    override fun getCurrencyRates() = Observable.fromCallable {
        return@fromCallable currencyDaoSession.getAll()
    }
}