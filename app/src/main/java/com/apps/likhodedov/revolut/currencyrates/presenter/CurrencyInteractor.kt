package com.apps.likhodedov.revolut.currencyrates.presenter

import com.apps.likhodedov.revolut.data.db.CurrencyRate
import com.apps.likhodedov.revolut.data.db.ICurrencyDbRepo
import com.apps.likhodedov.revolut.data.network.WebRate
import com.apps.likhodedov.revolut.data.network.ICurrencyRateWebRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/** Interactor, which providing currency rates. */
class CurrencyInteractor(
    /** Repository of currency rates at database. */
    private val currencyDbRepo: ICurrencyDbRepo,
    /** Repository of currency rates from internet. */
    private val currencyWebRepo: ICurrencyRateWebRepo
) : ICurrencyRateInteractor {

    override fun requestPeriodicUpdateCurrencies(): Observable<List<CurrencyRate>> {
        return Observable
            .interval(REQUEST_INIT_DELAY, REQUEST_PERIOD, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .flatMap {
                currencyWebRepo.getCurrencyRates()
                    .onErrorReturn { return@onErrorReturn ArrayList<WebRate>() }
            }
            .map { webRates ->
                webRates.map { webRate ->
                    CurrencyRate(
                        webRate.currencyCode,
                        webRate.rate
                    )
                }
            }
            .flatMap { currencyDbRepo.insertCurrencies(it) }
            .flatMap { currencyDbRepo.getCurrencyRates() }
            .observeOn(AndroidSchedulers.mainThread())
    }

    private companion object {
        private const val REQUEST_PERIOD = 1L
        private const val REQUEST_INIT_DELAY = 0L
    }
}