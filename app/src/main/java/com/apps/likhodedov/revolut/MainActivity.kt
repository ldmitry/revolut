package com.apps.likhodedov.revolut

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.apps.likhodedov.revolut.currencyrates.view.CurrencyRateFragment
import com.apps.likhodedov.revolut.currencyrates.view.ICurrencyRateView
import com.apps.likhodedov.revolut.di.activity.ActivityMainComponent
import com.apps.likhodedov.revolut.di.activity.IActivityMainComponentFactory
import com.apps.likhodedov.revolut.di.currencyfragment.CurrencyFragmentComponent
import com.apps.likhodedov.revolut.di.currencyfragment.CurrencyFragmentModule
import com.apps.likhodedov.revolut.di.currencyfragment.ICurrencyFragmentComponentFactory
import com.apps.likhodedov.revolut.utils.findComponentFactory

/** Main Activity of application. */
class MainActivity : AppCompatActivity(), ICurrencyFragmentComponentFactory {

    private lateinit var activityMainComponent: ActivityMainComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        activityMainComponent =
            findComponentFactory<IActivityMainComponentFactory>().createActivityMainComponent(this)
        activityMainComponent.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            val currencyFragment = CurrencyRateFragment()
            supportFragmentManager.beginTransaction().add(R.id.fragment_container, currencyFragment)
                .commit()
        }
    }

    override fun createCurrencyFragment(fragment: ICurrencyRateView): CurrencyFragmentComponent {
        return activityMainComponent.plus(CurrencyFragmentModule(fragment))
    }
}