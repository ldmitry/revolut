package com.apps.likhodedov.revolut.data.db

import androidx.room.*

/** Interface for interacting with currency rate database. */
@Dao
interface CurrencyDao {

    /** Get all currency rates from database. */
    @Query("SELECT * FROM currencyRate")
    fun getAll(): List<CurrencyRate>

    /**
     *  Insert [currencyRates] to database.
     *  if currency rate exist at database, it will be replaced.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(currencyRates: List<CurrencyRate>)
}