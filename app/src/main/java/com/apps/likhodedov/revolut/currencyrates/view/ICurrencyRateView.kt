package com.apps.likhodedov.revolut.currencyrates.view

/** Interface for view with currency rates. */
interface ICurrencyRateView {

    /**
     * Update currency rates, where
     * [currencyRateItems] - List of updated currency rate items.
     * [currentCurrencyRateItem] - item, which should be at top of list of rates.
     * [currentItemMoneyCount] - Money amount for [currentCurrencyRateItem].
     */
    fun updateCurrencyRates(
        currencyRateItems: List<CurrencyRateItem>,
        currentCurrencyRateItem: CurrencyRateItem,
        currentItemMoneyCount: Double
    )

    /** Set [visible] information, that currency rates is not available now. */
    fun setNoDataInfoVisible(visible: Boolean)

    /** Set [visible] currency rates.  */
    fun setCurrencyRatesInfoVisible(visible: Boolean)

    /** Set [visible] loader of currency rates. */
    fun setLoaderVisible(visible: Boolean)
}