package com.apps.likhodedov.revolut.utils

import android.app.Activity
import androidx.fragment.app.Fragment
import com.apps.likhodedov.revolut.di.IComponentFactory

/** Find factory for fragment. */
inline fun <reified T : IComponentFactory> Fragment.findComponentFactory(): T {
    if (this is T) {
        return this
    }

    var tempFragment = parentFragment
    while (tempFragment != null) {
        if (tempFragment is T) {
            return tempFragment
        } else {
            tempFragment = tempFragment.parentFragment
        }
    }

    return activity!!.findComponentFactory()
}

/** Find factory for activity. */
inline fun <reified T : IComponentFactory> Activity.findComponentFactory(): T {
    if (this is T) {
        return this
    }

    if (application is T) {
        return application as T
    }

    throw IllegalStateException()
}