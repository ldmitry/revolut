package com.apps.likhodedov.revolut.utils

import org.junit.Assert
import org.junit.Test

/** Tests for rates converter. */
class RateConverterUtilTest {

    private val calculationError = 0.01

    @Test
    fun rate_converter_with_no_money_test() {
        val money = 0.0
        val currentRate = 10.0
        val targetRate = 4.0

        val convertResult = convertMoney(currentRate, targetRate, money)
        Assert.assertEquals(money, convertResult, calculationError)
    }

    @Test
    fun rate_converter_with_same_rates_test() {
        val money = 57.124
        val currentRate = 10.0
        val targetRate = 10.0

        val convertResult = convertMoney(currentRate, targetRate, money)
        Assert.assertEquals(money, convertResult, calculationError)
    }

    @Test
    fun rate_converter_with_different_rates_test() {
        val money = 755.24
        val currentRate = 2.245
        val targetRate = 1.062

        val convertResult = convertMoney(currentRate, targetRate, money)
        val expectedResult = money / currentRate * targetRate
        Assert.assertEquals(expectedResult, convertResult, calculationError)
    }

    @Test
    fun rate_converter_to_higher_rate() {
        val money = 100.0
        var currentRate = 1.5
        var targetRate = 2.0

        var convertResult = convertMoney(currentRate, targetRate, money)
        Assert.assertTrue(convertResult > money)

        currentRate = 1.44
        targetRate = 1.45

        convertResult = convertMoney(currentRate, targetRate, money)
        Assert.assertTrue(convertResult > money)

        currentRate = 2.0
        targetRate = 2.001

        convertResult = convertMoney(currentRate, targetRate, money)
        Assert.assertTrue(convertResult > money)
    }

    @Test
    fun rate_converter_to_lower_rate() {
        val money = 100.0
        var currentRate = 0.63
        var targetRate = 0.55

        var convertResult = convertMoney(currentRate, targetRate, money)
        Assert.assertTrue(convertResult < money)

        currentRate = 1.11
        targetRate = 1.10

        convertResult = convertMoney(currentRate, targetRate, money)
        Assert.assertTrue(convertResult < money)

        currentRate = 52.2
        targetRate = 25.22

        convertResult = convertMoney(currentRate, targetRate, money)
        Assert.assertTrue(convertResult < money)

        currentRate = 1.404
        targetRate = 1.403

        convertResult = convertMoney(currentRate, targetRate, money)
        Assert.assertTrue(convertResult < money)
    }

    @Test
    fun rate_converter_twice_calculate_with_same_data_test() {
        val money = 125.52
        val currentRate = 6.23
        val targetRate = 1.52

        val convertResultFirst = convertMoney(currentRate, targetRate, money)
        val convertResultSecond = convertMoney(currentRate, targetRate, money)
        Assert.assertEquals(convertResultFirst, convertResultSecond, calculationError)
    }
}