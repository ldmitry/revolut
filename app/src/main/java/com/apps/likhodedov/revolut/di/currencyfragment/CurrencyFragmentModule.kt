package com.apps.likhodedov.revolut.di.currencyfragment

import android.content.Context
import com.apps.likhodedov.revolut.currencyrates.presenter.CurrencyInteractor
import com.apps.likhodedov.revolut.currencyrates.presenter.CurrencyRatePresenter
import com.apps.likhodedov.revolut.currencyrates.presenter.ICurrencyRatePresenter
import com.apps.likhodedov.revolut.currencyrates.presenter.CurrencyItemInfoProvider
import com.apps.likhodedov.revolut.currencyrates.view.ICurrencyRateView
import com.apps.likhodedov.revolut.data.db.ICurrencyDbRepo
import com.apps.likhodedov.revolut.data.network.ICurrencyRateWebRepo
import com.apps.likhodedov.revolut.di.FragmentScope
import dagger.Module
import dagger.Provides

/** Currency fragment module. */
@Module
class CurrencyFragmentModule(private val view: ICurrencyRateView) {


    /** Provide currency rates presenter. */
    @FragmentScope
    @Provides
    fun providePresenter(
        currencyDbRepo: ICurrencyDbRepo,
        currencyWebRepo: ICurrencyRateWebRepo,
        context: Context
    ): ICurrencyRatePresenter {
        return CurrencyRatePresenter(
            view,
            CurrencyInteractor(currencyDbRepo, currencyWebRepo),
            CurrencyItemInfoProvider(context)
        )
    }
}