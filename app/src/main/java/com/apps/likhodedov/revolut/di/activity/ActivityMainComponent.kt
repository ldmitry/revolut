package com.apps.likhodedov.revolut.di.activity

import com.apps.likhodedov.revolut.MainActivity
import com.apps.likhodedov.revolut.data.db.ICurrencyDbRepo
import com.apps.likhodedov.revolut.data.network.ICurrencyRateWebRepo
import com.apps.likhodedov.revolut.di.ActivityScope
import com.apps.likhodedov.revolut.di.currencyfragment.CurrencyFragmentComponent
import com.apps.likhodedov.revolut.di.currencyfragment.CurrencyFragmentModule
import dagger.Subcomponent

/** Activity component. */
@ActivityScope
@Subcomponent(modules = [ActivityMainModule::class])
interface ActivityMainComponent {

    /** Initialize [activity]. */
    fun inject(activity: MainActivity)

    /** Create component for currency fragment. */
    fun plus(module: CurrencyFragmentModule): CurrencyFragmentComponent

    /** Database repository. */
    fun currencyDbRepo(): ICurrencyDbRepo

    /** Web repository. */
    fun currencyWebRepo(): ICurrencyRateWebRepo
}