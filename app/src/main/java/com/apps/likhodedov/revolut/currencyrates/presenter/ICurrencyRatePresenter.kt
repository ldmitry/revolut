package com.apps.likhodedov.revolut.currencyrates.presenter

import com.apps.likhodedov.revolut.currencyrates.view.CurrencyRateItem

/** Interface of currency rate presenter. */
interface ICurrencyRatePresenter {

    /**
     * Clicked on currency rate item [currencyRateItem].
     * [moneyAmount] - Amount of money for this item.
     */
    fun onCurrencyRateClicked(currencyRateItem: CurrencyRateItem, moneyAmount: Double)

    /** Changed amount of [money] for current currency rate. */
    fun onCurrencyRateMoneyChangedByUser(money: Double)

    /** Currency rates scrolling. */
    fun onRatesScrolling()

    /** Currency rates scroll stopped. */
    fun onRatesScrollStopped()

    /** View created. */
    fun onViewCreated()

    /** View resumed. */
    fun onViewResumed()

    /** View paused. */
    fun onViewPaused()
}