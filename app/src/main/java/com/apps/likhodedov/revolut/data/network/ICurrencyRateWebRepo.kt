package com.apps.likhodedov.revolut.data.network

import io.reactivex.Observable

/** Interface for repository of currency rates from internet. */
interface ICurrencyRateWebRepo {

    /** Get currency rates. */
    fun getCurrencyRates(): Observable<ArrayList<WebRate>>
}