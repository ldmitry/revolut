package com.apps.likhodedov.revolut.di

import javax.inject.Scope

/** Activity Scope. */
@Scope
annotation class ActivityScope