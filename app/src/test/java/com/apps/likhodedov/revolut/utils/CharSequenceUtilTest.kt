package com.apps.likhodedov.revolut.utils

import org.junit.Assert
import org.junit.Test
import java.lang.IllegalArgumentException

/** Test charSequence util. */
class CharSequenceUtilTest {

    private val calculationError = 0.01

    @Test
    fun charSequence_toDouble_empty_test() {
        val charSequence: CharSequence = ""

        val expectedResult = 0.0

        Assert.assertEquals(expectedResult, charSequence.toDouble(), calculationError)
    }

    @Test
    fun charSequence_toDouble_with_decimal_places_test() {
        val charSequence: CharSequence = "24.25451"
        val expectedResult = 24.25451

        Assert.assertEquals(expectedResult, charSequence.toDouble(), calculationError)
    }

    @Test
    fun charSequence_toDouble_without_decimal_places_test() {
        val charSequence: CharSequence = "525"
        val expectedResult = 525.0

        Assert.assertEquals(expectedResult, charSequence.toDouble(), calculationError)
    }

    @Test
    fun charSequence_toDouble_with_dot_and_without_decimal_places_test() {
        val charSequence: CharSequence = "24."
        val expectedResult = 24.0

        Assert.assertEquals(expectedResult, charSequence.toDouble(), calculationError)
    }

    @Test(expected = IllegalArgumentException::class)
    fun charSequence_toDouble_with_text_test() {
        val charSequence: CharSequence = "text"
        charSequence.toDouble()
    }

    @Test(expected = IllegalArgumentException::class)
    fun charSequence_toDouble_number_with_symbols_test() {
        val charSequence: CharSequence = "24.#4"
        charSequence.toDouble()
    }
}