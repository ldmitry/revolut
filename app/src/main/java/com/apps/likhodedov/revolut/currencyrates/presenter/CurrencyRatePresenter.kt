package com.apps.likhodedov.revolut.currencyrates.presenter

import com.apps.likhodedov.revolut.currencyrates.view.CurrencyRateItem
import com.apps.likhodedov.revolut.currencyrates.view.ICurrencyRateView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.IllegalStateException
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

/** Currency rates presenter. */
class CurrencyRatePresenter(
    /** Currency rates view. */
    private val view: ICurrencyRateView,
    /** Interactor, for getting currency rates data. */
    private val interactor: ICurrencyRateInteractor,
    /** Provider for getting info about currency rate. */
    private val currencyItemInfoProvider: CurrencyItemInfoProvider
) : ICurrencyRatePresenter {

    /** State of currency rates. */
    private enum class RatesState {
        /** Rates loading. */
        LOADING,
        /** Rates ready to display. */
        READY,
        /** Rates updated. */
        UPDATED,
        /** Error while getting rates. */
        ERROR,
        /** Undefined state. */
        UNDEFINED
    }

    private val loadingTimerObservable =
        Observable.timer(MAX_LOADING_DATA_TIME_IN_SECONDS, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    private lateinit var currentCurrencyRateItem: CurrencyRateItem

    private lateinit var loadingTimerDisposable: Disposable

    private lateinit var currencyDisposable: Disposable

    private var ratesState: RatesState = RatesState.UNDEFINED

    private var currencyRateItems: MutableList<CurrencyRateItem> = ArrayList()

    private var currentMoneyInput = DEFAULT_MONEY_COUNT

    override fun onViewCreated() {
        handleRatesStateChanged(RatesState.LOADING)

        currencyDisposable = interactor.requestPeriodicUpdateCurrencies().subscribe { rates ->
            handleCurrenciesUpdated(rates.map { currencyItemInfoProvider.getCurrencyRateItem(it) })
        }

        loadingTimerDisposable = loadingTimerObservable.subscribe {
            handleRatesStateChanged(RatesState.ERROR)
        }
    }

    override fun onViewPaused() {
        currencyDisposable.dispose()
        loadingTimerDisposable.dispose()
    }

    override fun onViewResumed() {
        if (currencyDisposable.isDisposed) {
            currencyDisposable = interactor.requestPeriodicUpdateCurrencies().subscribe { rates ->
                handleCurrenciesUpdated(rates.map { currencyItemInfoProvider.getCurrencyRateItem(it) })
            }
        }

        if (ratesState == RatesState.LOADING && loadingTimerDisposable.isDisposed) {
            loadingTimerDisposable =
                loadingTimerObservable.subscribe {
                    handleRatesStateChanged(RatesState.ERROR)
                }
        }
    }

    override fun onRatesScrolling() {
        currencyDisposable.dispose()
    }

    override fun onRatesScrollStopped() {
        currencyDisposable = interactor.requestPeriodicUpdateCurrencies().subscribe { rates ->
            handleCurrenciesUpdated(rates.map { currencyItemInfoProvider.getCurrencyRateItem(it) })
        }
    }

    override fun onCurrencyRateClicked(currencyRateItem: CurrencyRateItem, moneyAmount: Double) {
        val movingItem = currencyRateItems.removeAt(currencyRateItems.indexOf(currencyRateItem))
        currentCurrencyRateItem = movingItem
        currentMoneyInput = moneyAmount
        currencyRateItems.add(0, movingItem)
        handleRatesStateChanged(RatesState.UPDATED)
    }

    override fun onCurrencyRateMoneyChangedByUser(money: Double) {
        currentMoneyInput = money
        handleRatesStateChanged(RatesState.UPDATED)
    }

    private fun handleCurrenciesUpdated(newCurrencyRateItems: List<CurrencyRateItem>) {
        if (currencyRateItems.isEmpty()) {
            if (newCurrencyRateItems.isNotEmpty()) {
                currencyRateItems = ArrayList(newCurrencyRateItems)
                currentCurrencyRateItem = currencyRateItems.first()
                loadingTimerDisposable.dispose()
                handleRatesStateChanged(RatesState.READY)
            }
        } else {
            if (newCurrencyRateItems.isNotEmpty()) {
                newCurrencyRateItems.forEach { item ->
                    val currencyItem = currencyRateItems.find { it == item }!!
                    currencyItem.rate = item.rate
                }
                handleRatesStateChanged(RatesState.UPDATED)
            }
        }
    }

    private fun handleRatesStateChanged(newRatesState: RatesState) {
        when (newRatesState) {
            RatesState.LOADING -> {
                view.setNoDataInfoVisible(false)
                view.setCurrencyRatesInfoVisible(false)
                view.setLoaderVisible(true)
            }
            RatesState.READY -> {
                view.setNoDataInfoVisible(false)
                view.setLoaderVisible(false)
                view.setCurrencyRatesInfoVisible(true)
                view.updateCurrencyRates(
                    currencyRateItems,
                    currentCurrencyRateItem,
                    currentMoneyInput
                )
            }
            RatesState.UPDATED -> {
                view.updateCurrencyRates(
                    currencyRateItems,
                    currentCurrencyRateItem,
                    currentMoneyInput
                )
            }
            RatesState.ERROR -> {
                view.setLoaderVisible(false)
                view.setCurrencyRatesInfoVisible(false)
                view.setNoDataInfoVisible(true)
            }
            else -> throw IllegalStateException("Illegal rates state")
        }
        ratesState = newRatesState
    }

    companion object {
        private const val DEFAULT_MONEY_COUNT: Double = 100.0
        private const val MAX_LOADING_DATA_TIME_IN_SECONDS = 4L
    }
}