package com.apps.likhodedov.revolut.currencyrates.presenter

import com.apps.likhodedov.revolut.data.db.CurrencyRate
import io.reactivex.Observable

/** Interface for currency rate interactor. */
interface ICurrencyRateInteractor {

    /** Request periodic update list of currency rates. */
    fun requestPeriodicUpdateCurrencies(): Observable<List<CurrencyRate>>
}