package com.apps.likhodedov.revolut.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey

/** Currency rate. */
@Entity
data class CurrencyRate(

    /** Currency code. */
    @PrimaryKey
    val currencyCode: String,

    /** Currency rate. */
    val rate: Double
)