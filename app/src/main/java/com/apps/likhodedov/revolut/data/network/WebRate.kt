package com.apps.likhodedov.revolut.data.network

/** Currency rate from internet. */
data class WebRate(
    /** Currency code. */
    val currencyCode: String,
    /** Currency rate. */
    val rate: Double
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is WebRate) return false

        return this.currencyCode == other.currencyCode
    }

    override fun hashCode(): Int {
        return currencyCode.hashCode()
    }
}