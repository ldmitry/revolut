package com.apps.likhodedov.revolut.utils

import kotlin.math.pow
import kotlin.math.round

/**
 * Convert money from one currency to another.
 * [baseRate] - rate for current currency.
 * [targetRate] - rate for wishful currency.
 * [money] - money amount to convert.
 */
fun convertMoney(
    baseRate: Double,
    targetRate: Double,
    money: Double
): Double {
    val result = money / baseRate * targetRate
    val decimalPlacesCount = 10.0.pow(DECIMAL_PLACES_COUNT)
    return round(result * decimalPlacesCount) / decimalPlacesCount
}

private const val DECIMAL_PLACES_COUNT = 2
