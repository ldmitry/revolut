package com.apps.likhodedov.revolut.di.app

import android.content.Context
import com.apps.likhodedov.revolut.CurrencyRateApplication
import com.apps.likhodedov.revolut.data.db.CurrencyDao
import com.apps.likhodedov.revolut.di.AppScope
import com.apps.likhodedov.revolut.di.activity.ActivityMainComponent
import com.apps.likhodedov.revolut.di.activity.ActivityMainModule
import dagger.Component
import javax.inject.Singleton

/** Application component. */
@AppScope
@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    /** Initialize Application [rateApplication]. */
    fun inject(rateApplication: CurrencyRateApplication)

    /** Create activity component. */
    fun activityMainComponent(module: ActivityMainModule): ActivityMainComponent

    /** Database dao session. */
    fun currencyDatabaseDaoSession(): CurrencyDao

    /** Context. */
    fun context(): Context
}