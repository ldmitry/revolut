package com.apps.likhodedov.revolut

import android.app.Application
import com.apps.likhodedov.revolut.di.activity.ActivityMainComponent
import com.apps.likhodedov.revolut.di.activity.ActivityMainModule
import com.apps.likhodedov.revolut.di.activity.IActivityMainComponentFactory
import com.apps.likhodedov.revolut.di.app.AppComponent
import com.apps.likhodedov.revolut.di.app.AppModule
import com.apps.likhodedov.revolut.di.app.DaggerAppComponent

/** Currency rates application. */
class CurrencyRateApplication : Application(), IActivityMainComponentFactory {

    private var appComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
        appComponent!!.inject(this)
    }

    override fun createActivityMainComponent(activity: MainActivity): ActivityMainComponent {
        return appComponent!!.activityMainComponent(ActivityMainModule(activity))
    }
}