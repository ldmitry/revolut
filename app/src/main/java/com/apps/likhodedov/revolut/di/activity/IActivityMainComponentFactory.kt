package com.apps.likhodedov.revolut.di.activity

import com.apps.likhodedov.revolut.MainActivity
import com.apps.likhodedov.revolut.di.IComponentFactory

/** Factory for activity component. */
interface IActivityMainComponentFactory : IComponentFactory {

    /** Create activity component. */
    fun createActivityMainComponent(activity: MainActivity): ActivityMainComponent
}