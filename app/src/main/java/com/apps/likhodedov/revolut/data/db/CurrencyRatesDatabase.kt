package com.apps.likhodedov.revolut.data.db

import androidx.room.Database
import androidx.room.RoomDatabase

/** Currency rate database. */
@Database(entities = [CurrencyRate::class], version = 1)
abstract class CurrencyRatesDatabase : RoomDatabase() {
    abstract fun currencyRatesDao(): CurrencyDao

    companion object {
        const val CURRENCY_DATABASE_NAME = "currency.db"
    }
}

