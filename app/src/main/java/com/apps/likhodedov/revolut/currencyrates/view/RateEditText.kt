package com.apps.likhodedov.revolut.currencyrates.view

import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent
import android.widget.EditText

/** EditText for currency rates, which handle clicks on system keys. */
class RateEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : EditText(context, attrs, defStyleAttr) {

    override fun onKeyPreIme(keyCode: Int, event: KeyEvent?): Boolean {
        return super.dispatchKeyEvent(event)
    }
}