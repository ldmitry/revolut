package com.apps.likhodedov.revolut.currencyrates.view

import android.graphics.drawable.Drawable

/** Item, which contains all info about currency rate. */
data class CurrencyRateItem(
    /** Country flag for this currency. */
    val countryOfCurrencyFlag: Drawable,
    /** Currency code. */
    val currencyCode: String,
    /** Currency name. */
    val currencyName: String,
    /** Currency rate. */
    var rate: Double
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }

        if (other !is CurrencyRateItem) {
            return false
        }

        return currencyCode == other.currencyCode
    }

    override fun hashCode(): Int {
        return currencyCode.hashCode()
    }

    override fun toString(): String {
        return "[$currencyCode ; $currencyName ; $rate]"
    }
}