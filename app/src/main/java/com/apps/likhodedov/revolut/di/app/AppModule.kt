package com.apps.likhodedov.revolut.di.app

import android.app.Application
import androidx.room.Room
import com.apps.likhodedov.revolut.data.db.CurrencyRatesDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/** Application module. */
@Module
class AppModule(
    private val application: Application
) {

    private var database: CurrencyRatesDatabase = Room.databaseBuilder(
        application.applicationContext,
        CurrencyRatesDatabase::class.java,
        CurrencyRatesDatabase.CURRENCY_DATABASE_NAME
    ).build()

    /** Provide database dao session. */
    @Singleton
    @Provides
    fun provideDatabaseDaoSession() = database.currencyRatesDao()

    /** Provide context. */
    @Singleton
    @Provides
    fun provideContext() = application.applicationContext!!
}