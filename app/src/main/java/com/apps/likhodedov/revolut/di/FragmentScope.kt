package com.apps.likhodedov.revolut.di

import javax.inject.Scope

/** Fragment Scope. */
@Scope
annotation class FragmentScope