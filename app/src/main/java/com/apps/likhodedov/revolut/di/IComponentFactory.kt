package com.apps.likhodedov.revolut.di

/** Marker interface for factories of components. */
interface IComponentFactory