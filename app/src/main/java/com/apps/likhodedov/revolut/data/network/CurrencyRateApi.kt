package com.apps.likhodedov.revolut.data.network

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.Retrofit
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import retrofit2.Converter
import java.lang.reflect.Type

/** API for getting currency rates from internet */
interface CurrencyRateApi {

    /** GET request to get list of currency rates. */
    @GET("latest?base=EUR")
    fun getCurrencyRates(): Observable<ArrayList<WebRate>>

    /** API Factory. */
    class Factory {
        companion object {

            /** Create currency rate API. */
            fun create(): CurrencyRateApi {
                val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(
                        createGsonConverterFactory(
                            object : TypeToken<ArrayList<WebRate>>() {}.type,
                            CurrencyRateDeserializer()
                        )
                    )
                    .baseUrl(RATES_URL)
                    .build()

                return retrofit.create(CurrencyRateApi::class.java)
            }

            private fun createGsonConverterFactory(
                type: Type,
                typeAdapter: Any
            ): Converter.Factory {
                val gsonBuilder = GsonBuilder()
                gsonBuilder.registerTypeAdapter(type, typeAdapter)
                val gson = gsonBuilder.create()

                return GsonConverterFactory.create(gson)
            }

            private const val RATES_URL = "https://revolut.duckdns.org"
        }

        private class CurrencyRateDeserializer : JsonDeserializer<ArrayList<WebRate>> {

            override fun deserialize(
                json: JsonElement?,
                typeOfT: Type?,
                context: JsonDeserializationContext?
            ): ArrayList<WebRate> {
                val currencies = ArrayList<WebRate>()
                val jsonObject = json!!.asJsonObject
                val jsonCurrencies = jsonObject.get("rates").asJsonObject.entrySet()

                jsonCurrencies.forEach {
                    currencies.add(WebRate(it.key, it.value.asDouble))
                }
                return currencies
            }
        }
    }
}