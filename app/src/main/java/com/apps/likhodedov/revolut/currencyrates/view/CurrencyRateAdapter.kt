package com.apps.likhodedov.revolut.currencyrates.view

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apps.likhodedov.revolut.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import android.view.inputmethod.InputMethodManager
import com.apps.likhodedov.revolut.utils.convertMoney
import com.apps.likhodedov.revolut.utils.toDouble
import java.math.BigDecimal

/** Adapter for currency rates. */
class CurrencyRateAdapter(
    /** App context. */
    private val context: Context,
    /** Adapter listener. */
    private val listener: Listener
) : RecyclerView.Adapter<CurrencyRateAdapter.CurrencyRateItemHolder>() {

    private val inputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    private lateinit var currentCurrencyRateItem: CurrencyRateItem

    private var currencyRateItems: List<CurrencyRateItem> = ArrayList()

    private var moneyCountInput: Double = NO_MONEY


    /**
     * Update currency rates.
     * [newCurrencyRateItems] - new list of currency rates.
     * [currencyRateItem] - currency item, which should be at top of list.
     * [currentMoney] - amount of money for current currency item.
     */
    fun updateCurrencyRates(
        newCurrencyRateItems: List<CurrencyRateItem>,
        currencyRateItem: CurrencyRateItem,
        currentMoney: Double
    ) {
        if (moneyCountInput != currentMoney) {
            moneyCountInput = currentMoney
        }

        if (currencyRateItems.isEmpty()) {
            currencyRateItems = ArrayList(newCurrencyRateItems)
            currentCurrencyRateItem = currencyRateItem
            notifyDataSetChanged()
        } else {
            if (currentCurrencyRateItem != currencyRateItem) {
                notifyItemMoved(currencyRateItems.indexOf(currencyRateItem), 0)
                currentCurrencyRateItem = currencyRateItem
            }

            currencyRateItems = ArrayList(newCurrencyRateItems)
            currencyRateItems.forEach {
                if (it != currentCurrencyRateItem) {
                    notifyItemChanged(currencyRateItems.indexOf(it))
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CurrencyRateItemHolder(
            LayoutInflater.from(context).inflate(
                R.layout.currency_item,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: CurrencyRateItemHolder, position: Int) {
        holder.currencyCode.text = currencyRateItems[position].currencyCode
        holder.currencyName.text = currencyRateItems[position].currencyName

        val calculatedMoney = convertMoney(
            currentCurrencyRateItem.rate,
            currencyRateItems[position].rate,
            moneyCountInput
        )

        val bigDecimal = BigDecimal(calculatedMoney.toString())

        holder.moneyCountEditor.setText(
            bigDecimal.toPlainString(),
            TextView.BufferType.EDITABLE
        )
        Glide
            .with(context)
            .load(currencyRateItems[position].countryOfCurrencyFlag)
            .fitCenter()
            .apply(RequestOptions.circleCropTransform())
            .into(holder.preview)
    }

    override fun getItemCount() = currencyRateItems.size

    /** View holder for currency rate. */
    inner class CurrencyRateItemHolder(
        /** view for currency rate item. */
        itemView: View
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener, TextWatcher {

        val preview: ImageView = itemView.findViewById(R.id.currency_image_preview)
        val currencyCode: TextView = itemView.findViewById(R.id.currency_code)
        val currencyName: TextView = itemView.findViewById(R.id.currency_name)
        val moneyCountEditor: RateEditText = itemView.findViewById(R.id.currency_value_editor)
        private val clickCatcher: View = itemView.findViewById(R.id.click_catcher)

        init {
            clickCatcher.setOnClickListener(this)
            moneyCountEditor.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    inputMethodManager.showSoftInput(
                        moneyCountEditor,
                        InputMethodManager.SHOW_FORCED
                    )
                } else {
                    inputMethodManager.hideSoftInputFromWindow(clickCatcher.windowToken, 0)
                    clickCatcher.visibility = View.VISIBLE
                    moneyCountEditor.removeTextChangedListener(this)
                }
            }

            moneyCountEditor.setOnKeyListener { _, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN
                    && (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_BACK)
                ) {

                    if (moneyCountEditor.text.isEmpty()) {
                        notifyItemChanged(adapterPosition)
                    }
                    moneyCountEditor.clearFocus()
                    return@setOnKeyListener true
                }
                return@setOnKeyListener false
            }
        }

        override fun onClick(view: View) {
            view.visibility = View.INVISIBLE

            moneyCountEditor.addTextChangedListener(this)

            listener.onCurrencyItemClicked(
                currencyRateItems[adapterPosition], convertMoney(
                    currentCurrencyRateItem.rate,
                    currencyRateItems[adapterPosition].rate,
                    moneyCountInput
                )
            )

            moneyCountEditor.requestFocus()
            moneyCountEditor.setSelection(moneyCountEditor.text.length)
        }

        override fun afterTextChanged(s: Editable?) {
            //nothing to do
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            //nothing to do
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            listener.onCurrentCurrencyMoneyChanged(s!!.toDouble())
        }
    }

    /** Adapter listener. */
    interface Listener {
        /** Click on currency rate item [currencyRateItem] with amount of money [money]. */
        fun onCurrencyItemClicked(currencyRateItem: CurrencyRateItem, money: Double)

        /** [money] amount changed for current currency rate item. */
        fun onCurrentCurrencyMoneyChanged(money: Double)
    }

    private companion object {
        private const val NO_MONEY = 0.0
    }
}